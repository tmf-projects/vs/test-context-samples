﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TestContextSamples
{
    [TestClass]
    public class MyTestSample
    {
        // TestContext используется для хранении информации о текущем Unit тесте
        // При тестировании вэб сервисов хранит URL
        // При тестировании ASP.NET приложений - предоставляет доступ к ASP странице
        // При использовании Data Driven тестов предоставляет доступ к источнику данных
        public TestContext TestContext { get; set; }

        [TestMethod]
        public void TestMethod1()
        {
            TestContext.WriteLine("TestRunDirectory {0}", TestContext.TestRunDirectory);
            TestContext.WriteLine("TestName {0}", TestContext.TestName);
            TestContext.WriteLine("CurrentTestOutcome {0}", TestContext.CurrentTestOutcome);
        }

        [TestCleanup]
        public void CleanUp()
        {
            TestContext.WriteLine("TestName (CleanUp) {0}", TestContext.TestName);
            TestContext.WriteLine("TestContext.CurrentTestOutcome (CleanUp) {0}", TestContext.CurrentTestOutcome);
        }
    }
}
